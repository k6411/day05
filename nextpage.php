<?php
$err = array();
function isDate($string)
{
    if (preg_match('/^([0-9]{1,2})\\-([0-9]{1,2})\\-([0-9]{4})$/', $string)) {
        return true;
    } else {
        return false;
    }
}
if (isset($_POST['submit'])) {

    error_reporting(E_ALL);
    // var_dump($_POST['gender']);
    session_start();
    if (empty($_POST['fullName'])) {
        $err['name'] = "Hãy nhập tên";
    } else {
        $_SESSION['fullName'] = $_POST['fullName'];
    }
    if (empty($_POST['gender'])) {
        $err['gender'] = "Hãy chọn giới tính";
    } else {
        $_SESSION['gender'] = $_POST['gender'];
    }
    if (empty($_POST['department'])) {
        $err['department'] = "Hãy chọn phân khoa";
    } else {
        $_SESSION['department'] = $_POST['department'];
    }
    if (empty($_POST['birthday'])) {
        $err['birthday'] = "Hãy nhập ngày sinh";
    } else if (!empty($_POST['birthday'])) {
        if (!isDate($_POST['birthday'])) {
            $err['birthday'] = "Hãy nhập đúng định dạng";
        } else {
            $_SESSION['birthday'] = $_POST['birthday'];
        }
    }

    if (empty($_POST['address'])) {
        $err['address'] = "Hãy nhập địa chỉ";
    } else {
        $_SESSION['address'] = $_POST['address'];
    }
    if (empty($_POST['img_encode'])) {
        $_SESSION["img_encode"] = $_POST['img_encode'];
    }
    if (empty($err)) {
        var_dump(1);
        $newURL = "validateSignUp.php";
        header('Location:' . $newURL);
    }
}

